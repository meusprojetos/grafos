package Busca;

import Grafo.Aresta;
import Grafo.Vertice;
import Grafo.Grafo;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.LinkedList;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import java.awt.Color;

public class Busca {

    public void DFS(Grafo grafo, Vertice vertice, LinkedList<Vertice> pilha, JTextPane panelResult) {
        vertice.setVisitado(true);
        Collections.sort(vertice.getListAdj());
        panelResult.setText(panelResult.getText() + "\n Visitando Vertice: " + vertice.getRotulo());
        for (int i = 0; i < vertice.getListAdj().size(); i++) {
            Vertice nextVertice = vertice.getAresta(i).getDestino();
            pilha.push(nextVertice);
            if (!nextVertice.isVisitado()) {
                pilha.remove(0);
                DFS(grafo, nextVertice, pilha, panelResult);
            }
        }
        if (pilha.isEmpty()) {
            Vertice nextVertice = grafo.searchNextVertice();
            if (nextVertice != null) {
                grafo.setConexo(false);
                panelResult.setText(panelResult.getText() + "\n\n -------- Novo Vertice Inicial: " + nextVertice.getRotulo() + "\n\n");
                DFS(grafo, nextVertice, pilha, panelResult);
            }
        }

    }

    public void BFS(Grafo grafo, Vertice vertice, ArrayList<Vertice> fila, JTextPane panelResult) {
        vertice.setVisitado(true);
        Collections.sort(vertice.getListAdj());
        panelResult.setText(panelResult.getText() + "\n Visitando Vertice: " + vertice.getRotulo());
        for (int i = 0; i < vertice.getListAdj().size(); i++) {
            Vertice verticeAdj = vertice.getAresta(i).getDestino();
            if (!verticeAdj.isVisitado()) {
                panelResult.setText(panelResult.getText() + "\n Adicionando na fila Vertice: " + verticeAdj.getRotulo());
                fila.add(verticeAdj);
            }
        }

        if (!fila.isEmpty()) {
            Vertice nextVertice = fila.get(0);
            fila.remove(0);
            panelResult.setText(panelResult.getText() + "\n\n Retirando da fila Vertice: " + nextVertice.getRotulo() + "\n\n");
            BFS(grafo, nextVertice, fila, panelResult);
        } else {
            Vertice nextVertice = grafo.searchNextVertice();
            if (nextVertice != null) {
                grafo.setConexo(false);
                panelResult.setText(panelResult.getText() + "\n\n -------- Novo Vertice Inicial: " + nextVertice.getRotulo() + "\n\n");
                BFS(grafo, nextVertice, fila, panelResult);
            }
        }
    }

    public void Dijkstra(Grafo grafo, Vertice v1, Vertice v2, JTextPane panelResult) {
        System.out.println("Inicial: "+v1.getRotulo());
        System.out.println("Final: "+v2.getRotulo());
        List<Vertice> menorCaminho = new ArrayList<>();
        Vertice verticeCaminho;
        Vertice atual;
        Vertice vizinho;
        List<Vertice> naoVisitados = new ArrayList<>();
        // Adiciona o Vertice de origem na lista do menor caminho
        menorCaminho.add(v1);

        // Colocando a distancias iniciais nos Vertices
        for (int i = 0; i < grafo.getVertices().size(); i++) {
            //Compara se o vertice do Grafo é o mesmo do de origem
            if (grafo.getVertices().get(i).getRotulo().equals(v1.getRotulo())) {
                //se for a Distancia é 0
                grafo.getVertices().get(i).setDistancia(0);
            } else {
                //ser for os outros 9999
                grafo.getVertices().get(i).setDistancia(9999);
            }
            // Insere o vertice na lista de vertices nao visitados
            naoVisitados.add(grafo.getVertices().get(i));
        }
        Collections.sort(naoVisitados);
        // O algoritmo continua ate que todos os vertices sejam visitados
        while (!naoVisitados.isEmpty()) {
            // Toma-se sempre o vertice com menor distancia, que eh o primeiro
            // da lista
            atual = naoVisitados.get(0);
            System.out.println("Pegou esse vertice:  " + atual.getRotulo());
            /*
             * Para cada Vertice adjacente (verticedestino da ligação) calcula a posivel distancia
             * somando a distancia do vertice atual com o peso da Ligacao. 
             * se a soma for menor que a distancia do vertice adjacente (vizinho), entao a distancia 
             * do vertice e atualizada
             */
            for (int i = 0; i < atual.getListAdj().size(); i++) {
//                Aresta a =
                vizinho = atual.getAresta(i).getDestino();
                System.out.println("Verificando Ligacao entre os vertices " + atual.getRotulo() + ": " + vizinho.getRotulo());
                if (!vizinho.isVisitado()) {
                    // Comparando a distância do vizinho com a possível
                    // distância
                    if (vizinho.getDistancia() > (atual.getDistancia() + atual.getAresta(i).getPeso())) {
                        vizinho.setDistancia((int) (atual.getDistancia() + atual.getAresta(i).getPeso()));
                        vizinho.setPai(atual);
                        /*
                         * Se o vizinho eh o vertice procurado, e foi feita uma
                         * mudanca na distancia, a lista com o menor caminho
                         * anterior eh apagada, pois existe um caminho menor
                         * vertices pais, ateh o vertice origem.
                         */
                        if (vizinho == v2) {
                            menorCaminho.clear();
                            verticeCaminho = vizinho;
                            menorCaminho.add(vizinho);
                            while (verticeCaminho.getPai() != null) {
                                menorCaminho.add(verticeCaminho.getPai());
                                verticeCaminho = verticeCaminho.getPai();
                            }
                            // Ordena a lista do menor caminho, para que ele
                            // seja exibido da origem ao destino.
                            Collections.sort(menorCaminho);
                        }
                    }
                }
            }
            // Marca o vertice atual como visitado e o retira da lista de nao
            // visitados
            atual.setVisitado(true);
            naoVisitados.remove(atual);
            /*
             * Ordena a lista, para que o vertice com menor distancia fique na
             * primeira posicao
             */
            Collections.sort(naoVisitados);
            System.out.println("Nao foram visitados ainda:" + naoVisitados);
        }
        panelResult.setText(panelResult.getText() + "Vertice Inicial: "+v1.getRotulo()+"\t Vertice Final: "+v2.getRotulo()+"\n");
        int custoTotal = 0;
        for (int i = 0; i < menorCaminho.size(); i++) {
            custoTotal += menorCaminho.get(i).getDistancia();
            panelResult.setText(panelResult.getText()+menorCaminho.get(i).getRotulo()+" - "+menorCaminho.get(i).getDistancia()+"\n");
        }
        panelResult.setText(panelResult.getText() + "Custo Total: "+custoTotal);
    }

    public void Coloracao(Grafo grafo, JTextPane panelResult) {
        String[] cores = {"azul", "vermelho", "verde", "amarelo", "branco", "preto", "rosa", "roxo", "marrom", "cinza"};
        grafo.ordernaPorGrau();
        ArrayList<Vertice> vertices = grafo.getVertices();
        ArrayList<Aresta> adjacentes;
        int corAtual = 1;
        vertices.get(0).setCor(corAtual);
        for (int i = 1; i < vertices.size(); i++) {
            corAtual = 1;
            adjacentes = vertices.get(i).getListAdj();
            for (int a = 0; a < adjacentes.size();) {
                if (adjacentes.get(a).getDestino().getCor() == corAtual) {
                    corAtual++;
                    a = 0;
                } else {
                    a++;
                }
            }
            vertices.get(i).setCor(corAtual);
        }
        grafo.ordernaPorVertice();
        for (int i = 0; i < grafo.getVertices().size(); i++) {
            desenha(panelResult, grafo.getVertices().get(i).getRotulo(), grafo.getVertices().get(i).getCor());
        }
    }
    
    public void desenha(JTextPane panelResult, String string, int colorNumber){
        String[] cores = {"Azul", "Vermelho", "Verde", "Amarelo", "Preto", "Rosa", "Laranja", "Cinza"};
        Color cor = Color.BLACK;
        switch(colorNumber){
            case 0:
                cor = Color.BLUE;
                break;
            case 1:
                cor = Color.RED;
                break;
            case 2:
                cor = Color.GREEN;
                break;
            case 3:
                cor = Color.YELLOW;
                break;
            case 4:
                cor = Color.BLACK;
                break;
            case 5:
                cor = Color.PINK;
                break;
            case 6:
                cor = Color.ORANGE;
                break;
            case 7:
                cor = Color.GRAY;
                break;   
        }
        StyledDocument document = panelResult.getStyledDocument();
        Style style = panelResult.addStyle("", null);
        StyleConstants.setForeground(style, cor);
        try {
            document.insertString(document.getLength(), string+" - "+cores[colorNumber]+"\n", style);
        }catch (BadLocationException e){}
    };
    
    public void caxeiroViajante(Grafo grafo, String origem, String destino, JTextPane panelResult){
        int tamLinha = grafo.getNumVertices();
        int tamColuna = grafo.getNumVertices();

        int vOrigem;
        int distMaisProxima[], verticeMProximo[];
        int matrizCaixeiro[][] = new int[tamLinha][tamLinha];
        int matriz[][] = grafo.getMatrizAdjacencia();
        int soma;

        //zera matriz caixeiro = 0
        zeraMatriz(matrizCaixeiro, tamLinha);
        
        //zera vetor de vertice e distancia = -1
        distMaisProxima = new int[tamLinha + 1];
        verticeMProximo = new int[tamLinha + 1];
        for (int i = 0; i < tamLinha + 1; i++) {
            distMaisProxima[i] = Integer.MAX_VALUE;
            verticeMProximo[i] = -1;
        }

        //receber as ligações na matriz
        for (int l = 0; l < tamLinha; l++) {
            for (int c = 0; c < tamColuna; c++) {
                //se tem ligacao, matriz recebe valor
                matrizCaixeiro[l][c] = matriz[l][c];
            }
        }

        //desenha matriz do usuário
        desenhaMatrizCaixeiro(matrizCaixeiro, grafo.getInfinito(), panelResult);

        //inicia algoritmo caixeiro viajante
        //Método do vértice adjacente mais próximo
//        System.out.println("Raiz: " + raiz);
        int raiz = getPosVertice(origem, grafo);
        int vdestino = getPosVertice(destino, grafo);
        vOrigem = raiz;
        verticeMProximo[0] = raiz;
        distMaisProxima[0] = 0;
        soma = 0;
        while (cicloCompleto(distMaisProxima, grafo.getNumVertices()) == false) {

            //Contagem para saber a posição "inicial" que se está estudando
            for (int cont = 0; cont < tamLinha; cont++) {

                //Laço para itentificar o adjacente mais próximo
                for (int l = 0; l < tamLinha; l++) {

                    //Quando a dist. é maior que a dist. da matriz
                    //e o vértice dessa dist. não foi estudado, entra na função
                    if (distMaisProxima[cont] > matrizCaixeiro[vOrigem][l]
                            && verticeEncaminhado(verticeMProximo, l, grafo.getNumVertices(), panelResult) == false) {

                        //Guarda no vetor a menor distancia e o vetor dela
                        distMaisProxima[cont] = matrizCaixeiro[vOrigem][l];
                        verticeMProximo[cont] = l;
                    }
                }

                //Guarda o vértice de menor distancia para o próximo loop
                vOrigem = verticeMProximo[cont];

                //Guarda a distancia percorrida
                soma = soma + distMaisProxima[cont];

//                //Desenha os vetores dos vértices e das distancias
//                desenhaVetorCaixeiro(verticeMProximo, soma);
//                desenhaVetorCaixeiro(distMaisProxima, soma);
            }

            //Recebe a distancia entre o ultimo vertice estudado e a raiz
            distMaisProxima[tamLinha] = matrizCaixeiro[vOrigem][raiz];
            //Ultimo vertice recebe a raiz
            verticeMProximo[tamLinha] = raiz;
            //Guarda a distancia percorrida
            soma = soma + distMaisProxima[tamLinha];
        }

        //Desenha o vetor do menor caminho encontrado
        //com seus vertices e soma das distancias
//        painelResultados.append("\n\nMenor caminho:");
        desenhaVetorCaixeiro(verticeMProximo, soma, grafo, raiz, vdestino, distMaisProxima, panelResult);
        
    };
    
        //Zera matriz
    public void zeraMatriz(int matriz[][], int numVertices) {
        for (int i = 0; i < numVertices; i++) {
            for (int j = 0; j < numVertices; j++) {
                matriz[i][j] = 0;
            }
        }
    };
    
    public void desenhaMatrizCaixeiro(int matrizCaixeiro[][], int infinito, JTextPane panelResult) {
        for (int l = 0; l < matrizCaixeiro.length; l++) {
            for (int c = 1; c < matrizCaixeiro.length; c++) {
                if (matrizCaixeiro[l][c - 1] != infinito) {
                    panelResult.setText(panelResult.getText()+matrizCaixeiro[l][c - 1] + " | ");
//                    System.out.print(matrizCaixeiro[l][c - 1] + " | ");
                } else {
                    panelResult.setText(panelResult.getText() +"X | ");
//                    System.out.print("X | ");
                }
            }
            panelResult.setText(panelResult.getText() +"\n");
//            System.out.println("");
        }
    };
    
    public boolean cicloCompleto(int vetorProximo[], int numVertices) {
        for (int i = 0; i < numVertices; i++) {
            if (vetorProximo[i] == Integer.MAX_VALUE) {
//                System.out.println("Campo não preenchido: " + i);
                return false;
            }
        }
        return true;
    };
    
    //Verifica se o vértice está no vetor dos mais proximos.
    public boolean verticeEncaminhado(int vetor[], int v, int numVertices, JTextPane panelResult) {
        for (int i = 0; i < numVertices; i++) {
            if (vetor[i] == v) {
                panelResult.setText(panelResult.getText()+"\nVértice estudado: " + v + " = " + vetor[i]);
//                System.out.println("Vértice estudado: " + v + " = " + vetor[i]);
                return true;
            }
        }
        panelResult.setText(panelResult.getText()+"\nVértice não estudado: " + v);
//        System.out.println("Vértice não estudado: " + v);
        return false;
    };
    
    //Desenha os vetores com a soma das distancias
    public void desenhaVetorCaixeiro(int vetor[], int soma, Grafo grafo, int raiz, int vdestino, int distMaisProxima[], JTextPane panelResult) {
        int somaCaminho = 0;
//        System.out.println("\n");
//        System.out.println("Ciclo: \n");
        panelResult.setText(panelResult.getText()+"\nCiclo: \n");
        for (int i = 0; i < grafo.getNumVertices() + 1; i++) {
//            System.out.print(grafo.getVertices().get(vetor[i]).getRotulo() + " | ");
            panelResult.setText(panelResult.getText()+grafo.getVertices().get(vetor[i]).getRotulo() + " | ");
        }
//        System.out.println("CustoTotal: " + soma);
//        System.out.println("\nCaminho: \n");
        panelResult.setText(panelResult.getText()+"\nCustoTotal: " + soma+"\n Caminho: \n");
        for (int i = 0; i < grafo.getNumVertices(); i++) {
//            System.out.print(grafo.getVertices().get(vetor[i]).getRotulo() + " | ");
            panelResult.setText(panelResult.getText()+grafo.getVertices().get(vetor[i]).getRotulo() + " | ");
            somaCaminho += distMaisProxima[i];
            if(vetor[i] == vdestino){
                break;
            }
        }
//        System.out.println("\nCusto: "+somaCaminho);
        panelResult.setText(panelResult.getText()+"\nCusto: "+somaCaminho);
    };
//    verticeMProximo, soma, grafo, raiz, vdestino, distMaisProxima
    public int getPosVertice(String rotulo, Grafo grafo){
        int pos = 0;
        for (int i = 0; i < grafo.getNumVertices(); i++) {
            if(grafo.getVertices().get(i).getRotulo().toLowerCase().equals(rotulo.toLowerCase())){
                pos = i;
                break;
            }
        }
        return pos;
    };

}
