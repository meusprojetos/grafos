package Principal;

import AEstrela.Mapa;
import AEstrela.Quadrado;
import Grafo.Grafo;
import java.io.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class LeitorXml {

    public static String caminho;

    public static Grafo grafoFromXML(File file) {
        Grafo grafo = new Grafo();
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            doc.getDocumentElement().normalize();
            NodeList nodes = doc.getElementsByTagName("xml");
            if (nodes.getLength() > 0) {
                Node node = nodes.item(0);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    Boolean.parseBoolean(element.getAttribute("encoding"));
                }
            }
            // VERIFICA SE É DIRIGIDO
            nodes = doc.getElementsByTagName("Grafo");
            if (nodes.getLength() > 0) {
                Node node = nodes.item(0);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    boolean dirigido = Boolean.parseBoolean(element.getAttribute("dirigido"));
                    grafo.setDirigido(dirigido);
                }
            }
            nodes = doc.getElementsByTagName("Vertice");
            for (int i = 0; i < nodes.getLength(); i++) {
                Node node = nodes.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    int relId = Integer.parseInt(element.getAttribute("relId"));
                    String rotulo = element.getAttribute("rotulo");
                    int posX = Integer.parseInt(element.getAttribute("posX"));
                    int posY = Integer.parseInt(element.getAttribute("posY"));
                    grafo.addVertice(posX, posY, rotulo, relId);
                }
            }
            // gera matriz atribuindo valor infinito para todos os caminhos
            grafo.criaMatrizAdjacencia();
            nodes = doc.getElementsByTagName("Aresta");
            for (int i = 0; i < nodes.getLength(); i++) {
                Node node = nodes.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    int idVertice1 = Integer.parseInt(element.getAttribute("idVertice1"));
                    int idVertice2 = Integer.parseInt(element.getAttribute("idVertice2"));
                    String peso = element.getAttribute("peso");
                    int peso2 = Integer.parseInt(peso.substring(0, peso.indexOf('.')));
                    grafo.addAresta(idVertice1, idVertice2, peso2);
                }
            }
            if(grafo.getNumVertices() > 0){
                return grafo;
            }else{
                return null;
            }
        } catch (Exception ex) {
            System.err.println(ex);
            return null;
        }
    }

    public static Mapa mapaFromXML(File file) {
        Mapa mapa = new Mapa();
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            doc.getDocumentElement().normalize();
            NodeList nodes;

            nodes = doc.getElementsByTagName("LINHAS");
            if (nodes.getLength() > 0) {
                Node node = nodes.item(0);
                mapa.setLinhas(Integer.parseInt(node.getTextContent()));
            }
            nodes = doc.getElementsByTagName("COLUNAS");
            if (nodes.getLength() > 0) {
                Node node = nodes.item(0);
                mapa.setColunas(Integer.parseInt(node.getTextContent()));
            }
            mapa.createArea();
            nodes = doc.getElementsByTagName("INICIAL");
            if (nodes.getLength() > 0) {
                Node node = nodes.item(0);
                String nodeText = node.getTextContent();
                mapa.setOrigem(new Quadrado(Integer.parseInt(nodeText.substring(0, 1)), Integer.parseInt(nodeText.substring(2, 3))));
            }
            nodes = doc.getElementsByTagName("FINAL");
            if (nodes.getLength() > 0) {
                Node node = nodes.item(0);
                String nodeText = node.getTextContent();
                mapa.setDestino(new Quadrado(Integer.parseInt(nodeText.substring(0, 1)), Integer.parseInt(nodeText.substring(2, 3))));
            }
            nodes = doc.getElementsByTagName("MURO");
            if (nodes.getLength() > 0) {
                for (int i = 0; i < nodes.getLength(); i++) {
                    Node node = nodes.item(i);
                    String nodeText = node.getTextContent();
                    mapa.setMuro(Integer.parseInt(nodeText.substring(0, 1)), Integer.parseInt(nodeText.substring(2, 3)));
                }
            }
            if(mapa.getLinhas() > 0){
                return mapa;
            }else{
                return null;
            }
        } catch (Exception ex) {
            System.err.println(ex);
            return null;
        }
    }
}
