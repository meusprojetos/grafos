package Grafo;

import java.util.ArrayList;

public class Vertice implements Comparable<Vertice> {

    private int id;
    private String rotulo;
    private int posX;
    private int posY;
    private ArrayList<Aresta> listAdj;
    private boolean visitado;
    private int distancia;
    private Vertice pai;
    private int grau;
    private int cor;
    
    public Vertice(int id, String rotulo, int posX, int posY) {
        this.id = id;
        this.rotulo = rotulo;
        this.posX = posX;
        this.posY = posY;
        this.visitado = false;
        this.listAdj = new ArrayList<Aresta>();
        this.distancia = 0;
        this.grau = 0;
        this.cor = 1000000000;
    }

    public int getCor() {
        return cor;
    }

    public void setCor(int cor) {
        this.cor = cor;
    }

    public Vertice getPai() {
        return pai;
    }

    public void setPai(Vertice pai) {
        this.pai = pai;
    }

    public int getDistancia() {
        return distancia;
    }

    public void setDistancia(int distancia) {
        this.distancia = distancia;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRotulo() {
        return rotulo;
    }

    public void setRotulo(String rotulo) {
        this.rotulo = rotulo;
    }

    public int getPosX() {
        return posX;
    }

    public void setPosX(int posX) {
        this.posX = posX;
    }

    public int getPosY() {
        return posY;
    }

    public void setPosY(int posY) {
        this.posY = posY;
    }

    public ArrayList getListAdj() {
        return listAdj;
    }

    public int getGrau() {
        return grau;
    }

    public void addGrau(){
        this.grau++;
    }
    
    
    public Aresta getAresta(int index) {
        return listAdj.get(index);
    }

    public void setListAdj(ArrayList listAdj) {
        this.listAdj = listAdj;
    }

    public boolean isVisitado() {
        return visitado;
    }

    public void setVisitado(boolean visitado) {
        this.visitado = visitado;
    }

    public void addListAdj(Aresta aresta) {
        this.listAdj.add(aresta);
    }

    @Override
    public int compareTo(Vertice vertice) {
        if (getId()< vertice.getId()) {
            return -1;
        } else if (getId() == vertice.getId()) {
            return 0;
        }
        return 1;

    }

}
