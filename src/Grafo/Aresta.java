package Grafo;


public class Aresta implements Comparable<Aresta> {

    private int id;
    private Vertice origem;
    private Vertice destino;
    private double peso;

    public Aresta(int id, Vertice origem, Vertice destino, double peso) {
        this.id = id;
        this.origem = origem;
        this.destino = destino;
        this.peso = peso;
    }

    public int getId() {
        return id;
    }

    public Vertice getOrigem() {
        return origem;
    }

    public Vertice getDestino() {
        return destino;
    }

    public double getPeso() {
        return peso;
    }

    @Override
    public int compareTo(Aresta aresta) {
        if (this.getDestino().getId() < aresta.getDestino().getId()) {
            return -1;
        } else if ( this.getDestino().getId() == aresta.getDestino().getId()) {
            return 0;
        }
        return 1;
    }

}
