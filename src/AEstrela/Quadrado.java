package AEstrela;

public class Quadrado {

    private int custoG;
    private int custoH;
    private int c;
    private int l;
    private boolean visitado;
    private boolean muro;

    public boolean isMuro() {
        return muro;
    }

    public void setMuro(boolean muro) {
        this.muro = muro;
    }
    
    
    public Quadrado(int l, int c) {
        this.l = l;
        this.c = c;
        this.custoG = 0;
        this.custoH = 0;
        this.visitado = false;
        this.muro = false;
    }

    public boolean isVisitado() {
        return visitado;
    }

    public void setVisitado(boolean visitado) {
        this.visitado = visitado;
    }

    public int getCustoF() {
        return custoG + custoH;
    }

    public int getCustoG() {
        return custoG;
    }

    public void setCustoG(int custoG) {
        this.custoG = custoG;
    }

    public int getCustoH() {
        return custoH;
    }

    public void setCustoH(int custoH) {
        this.custoH = custoH;
    }

    public int getC() {
        return c;
    }

    public int getL() {
        return l;
    }
}
