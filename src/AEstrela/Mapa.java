package AEstrela;

public class Mapa {

    private int linhas;
    private int colunas;
    private Quadrado[][] area;
    private Quadrado origem;
    private Quadrado destino;

    public Mapa() {
    }    

    public int getLinhas() {
        return linhas;
    }

    public void setLinhas(int linhas) {
        this.linhas = linhas;
    }

    public int getColunas() {
        return colunas;
    }

    public void setColunas(int colunas) {
        this.colunas = colunas;
    }

    public Quadrado[][] getArea() {
        return area;
    }

    public void setArea(Quadrado[][] mapa) {
        this.area = mapa;
    }
    
    public Quadrado getQuadradoInArea(int x, int y){
        return this.area[x][y];
    }
    
    public void createArea(){
        this.area = new Quadrado[this.linhas][this.colunas];
        for (int l = 0; l < this.linhas; l++) {
            for (int c = 0; c < this.colunas; c++) {
                this.area[l][c] = new Quadrado(l, c);
            }
        }
    };

    public Quadrado getOrigem() {
        return origem;
    }

    public void setOrigem(Quadrado origem) {
        this.origem = origem;
    }

    public Quadrado getDestino() {
        return destino;
    }

    public void setDestino(Quadrado destino) {
        this.destino = destino;
    }

    public void setMuro(int x, int y){
        this.area[x][y].setMuro(true);
    }
    
    public boolean isOrigem(Quadrado quadrado){
        if(this.origem.getL() == quadrado.getL() && this.origem.getC() == quadrado.getC()){
            return true;
        }
        return false;
    }
    
    public boolean isDestino(Quadrado quadrado){
        if(this.destino.getL() == quadrado.getL() && this.destino.getC() == quadrado.getC()){
            return true;
        }
        return false;
    }
}
