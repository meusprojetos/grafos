
package AEstrela;

import java.awt.Color;
import java.util.ArrayList;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

public class AEstrela {
    
    public void iniciar(Mapa mapa, JTextPane panelResult){
        ArrayList<Quadrado> caminho = new ArrayList<>();
        Quadrado atual = mapa.getOrigem();
        mapa.getQuadradoInArea(atual.getL(), atual.getC()).setVisitado(true);
        caminho.add(atual);
        calcularCaminho(mapa, atual, caminho);
        Color cor;
        for (int l = 0; l < mapa.getLinhas(); l++) {
            for (int c = 0; c < mapa.getColunas(); c++) {
                if(!mapa.getQuadradoInArea(l, c).isMuro() && !mapa.getQuadradoInArea(l, c).isVisitado()){
                    cor = Color.LIGHT_GRAY;
                }else{
                    if(mapa.getQuadradoInArea(l, c).isVisitado()){
                        if(mapa.isOrigem(mapa.getQuadradoInArea(l, c))){
                            cor = Color.BLUE;
                        }else{
                            if(mapa.isDestino(mapa.getQuadradoInArea(l, c))){
                                cor = Color.GREEN;
                            }else{
                                cor = Color.RED;
                            }
                        }
                    }else{
                        cor = Color.BLACK;
                    }
                }
                desenha(panelResult, "X  ", cor);
            }
            desenha(panelResult, "\n", Color.BLACK);
        }
        desenha(panelResult, "\n Caminho: \n", Color.BLACK);
        for (Quadrado quadrado : caminho) {
            desenha(panelResult, (quadrado.getL() + 1) + "," + (quadrado.getC() + 1) + " = " + quadrado.getCustoF() + ";  ", Color.BLACK);
        }
    }
    
    public void desenha(JTextPane panelResult, String string, Color cor){
        StyledDocument document = panelResult.getStyledDocument();
        Style style = panelResult.addStyle("", null);
        StyleConstants.setForeground(style, cor);
        try {
            document.insertString(document.getLength(), string,style);
        }catch (BadLocationException e){}
    }
    
    public void calcularCaminho(Mapa mapa, Quadrado atual, ArrayList<Quadrado> caminho){
        Quadrado temp;
        Quadrado ultimo = null;
        for (int l = atual.getL() - 1; l <= atual.getL() + 1; l++) {
            for (int c = atual.getC() - 1; c <= atual.getC() + 1; c++) {
                if(l >= 0 && c >=0){
                    temp = mapa.getQuadradoInArea(l, c);
                    if((l != atual.getL() || c != atual.getC()) && !temp.isMuro() && !temp.isVisitado()){
                        calcularDistancia(mapa, atual, temp, l, c);
                        if(ultimo == null){
                            ultimo = temp;
                        }else{
                            if(ultimo.getCustoF() > temp.getCustoF()){
                                ultimo = temp;
                            }

                        }
                    }
                }
            }
        }
        ultimo.setVisitado(true);
        caminho.add(ultimo);
        if(ultimo.getC() != mapa.getDestino().getC() || ultimo.getL() != mapa.getDestino().getL()){
            calcularCaminho(mapa, ultimo, caminho);
        }else{
            System.out.println("ACABEI");
        }
        
    }
    
    public void calcularDistancia(Mapa mapa, Quadrado atual, Quadrado temp,int l, int c){
        int h = 10 * (Math.abs(c - mapa.getDestino().getC()) + Math.abs(l - mapa.getDestino().getL()));;
        temp.setCustoH(h);
        if(l - atual.getL() == 0 || c - atual.getC() == 0){
            temp.setCustoG(atual.getCustoG() + 10);
        }else{
            temp.setCustoG(atual.getCustoG() + 14);
        }
    }
    
}
